# Installation
1. Create a new folder for your project.
2. Initialize an empty Git repository using<br>
   `git init`
3. Use composer `composer require zubzet/framework`
4. Login with your account if needed.
5. You are required to have your php environment variable set. Use `php -v` to check. Fix this problem first, if you get an error.
6. Navigate to your project in your browser. (Don't forget to use a webserver)
7. Open the framework folder
8. Run `installer.php`
9. Make sure your mysql capable service of choice is running.
10. Fill out all the inputs and click **Install**.